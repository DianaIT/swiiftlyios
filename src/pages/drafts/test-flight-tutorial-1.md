---
title:  "Cómo Subir tu App de iOS a TestFlight"
description: "Capitulo 1: Crear una cuenta en  App Store Connect"
pubDate: "Aug 21 2023"
heroImage: "/appstates.webp"
layout: "../../layouts/BlogPost.astro"
---

# 1️⃣ Cómo Subir tu App de iOS a TestFlight

### Capitulo 1: Crear una cuenta en  App Store Connect

Lo primero que tendrás que hacer es crearte una cuenta de Desarrollador de Apple