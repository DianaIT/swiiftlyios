---
title:  "Cómo Subir tu App de iOS a TestFlight"
description: "Porque... ¿Qué es una app sin usuarios?"
pubDate: "Aug 21 2023"
heroImage: "/appstates.webp"
layout: "../../layouts/BlogPost.astro"
---

# Cómo Subir tu App de iOS a TestFlight

¡Hola, entusiastas de la tecnología! Si estás aquí, es muy probable que hayas trabajado duro diseñando, desarrollando y perfeccionando tu aplicación para iOS. Pero... **¿Qué es una app sin usuarios?**

Uno de los handicaps más importantes a la hora de adentrarte en el desarrollo de aplicaicones en iOS es la dificultad de poder mostrar los resultados de una forma sencilla sin necesidad de pasar por la App Store. 

Android parece ese campo de amapolas dónde con un simple archivo .apk puedes tener un aplicación lista en cualquier dispositivo. Pero en iOS es otro cantar. 

Puedes cargar la app desde XCode, pero solo te durará una semana, Apple y sus políticas. Y sus precios, porque para poder compartir la app con familiares, amigos o un grupo reducido de target específico, tienes que pasar por caja. Exactamente 99 dólares después **TestFlight** será tu aliado para poder compartir tu app con quien tu quieras.

Así que vamos a ver como se hace toda esta vaina ¿Te vienes?

⚠️ Disclaimer: Nunca he hecho esto, voy a aprender a medida que completo esta serie de artículos, no pretendo sentar cátedra de nada, solo ir contando mis desventuras en forma de fallos y aciertos con TestFlight. 

- [Capitulo 1: Crear una cuenta en  App Store Connect](./test-flight-tutorial-1)
