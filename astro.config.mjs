import { defineConfig } from 'astro/config';
import mdx from '@astrojs/mdx';
import sitemap from '@astrojs/sitemap';

// https://astro.build/config
export default defineConfig({
	site: 'https://dianait.dev',
	integrations: [mdx(), sitemap()],
	type: 'module',
    vite: {
        define: {
            'import.meta.env': process.env
        },
        envDir: '.'
    }
});

